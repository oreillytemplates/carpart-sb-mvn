package com.oreillyauto.service;

import java.util.List;

import com.oreillyauto.domain.Carpart;

public interface CarpartsService {
    public Carpart getCarpartByPartNumber(String partNumber);
    public List<Carpart> getCarparts();
    public Carpart getCarpart(String partNumber) throws Exception;
}
