package com.oreillyauto.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.oreillyauto.dao.CarpartsRepository;
import com.oreillyauto.domain.Carpart;
import com.oreillyauto.service.CarpartsService;

@Service("CarpartsService") 
public class CarpartsServiceImpl implements CarpartsService {
    @Autowired
    private CarpartsRepository carpartsRepo;
    
    @Override
    public Carpart getCarpartByPartNumber(String partNumber) {
        return carpartsRepo.getCarpartByPartNumber(partNumber);
    }
    
    @Override
    public List<Carpart> getCarparts(){
        Carpart myPart = (Carpart)(carpartsRepo.findById("H4666ST")).get();
        System.out.println(myPart);
        List<Carpart> allParts = (List<Carpart>) carpartsRepo.findAll();
        return allParts;
    }

	@Override
	public Carpart getCarpart(String partNumber) throws Exception {
		return carpartsRepo.getCarpart(partNumber);
	}
}

