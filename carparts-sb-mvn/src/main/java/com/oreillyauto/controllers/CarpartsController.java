package com.oreillyauto.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;

import com.oreillyauto.domain.Carpart;
import com.oreillyauto.service.CarpartsService;

@Controller
public class CarpartsController {
	
	@Autowired
	CarpartsService carpartsService;
	
	@ResponseBody
	@GetMapping(value = { "/test" })
	public String carparts() throws Exception {
		Carpart carpart = carpartsService.getCarpart("H4666ST");
		return (carpart == null) ? "NULL!" : carpart.toString();
	}
	
	@GetMapping(value = { "/carparts/electrical/{partNumber}", "/carparts/engine/{partNumber}", "/carparts/other/{partNumber}" })
	public String getCarpart(@PathVariable String partNumber, Model model) throws Exception {
		String error = "";
		Carpart carpart = carpartsService.getCarpartByPartNumber(partNumber);
		
		if (carpart == null) {
			error = "Sorry, cannot find part number " + partNumber;
		}
			
		model.addAttribute("carpart", ((carpart == null) ? new Carpart() : carpart));
		model.addAttribute("active", partNumber);
		model.addAttribute("error", error);
		return "carparts";
	}
	
}
