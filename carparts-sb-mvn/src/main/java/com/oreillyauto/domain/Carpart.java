package com.oreillyauto.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

@Entity
@Table(name="carparts")
public class Carpart implements Serializable{
        private static final long serialVersionUID = -515023023200593312L;
        
        @Id
        @Column(name = "part_number", columnDefinition = "VARCHAR(64)")
        private String partNumber;
        
        @Column(name = "line", columnDefinition = "VARCHAR(256)")
        private String line;
        
        @Column(name = "title", columnDefinition = "VARCHAR(256)")
        private String title;
        
        @Column(name = "description", columnDefinition = "VARCHAR(256)")
        private String description;
        
        @Column(name = "image_name", columnDefinition = "VARCHAR(256)")
        private String imageName;
        
        @Transient
        private Map<String, String> detailsMap = new HashMap<String, String>();
        
        @OneToMany(fetch = FetchType.EAGER, mappedBy = "carpart")
        private List<CarpartDetail> carpartDetailsList = new ArrayList<CarpartDetail>();
        
        @LazyCollection(LazyCollectionOption.FALSE)
        @OneToMany(mappedBy = "carpart")
        private List<StoreLocation> storeLocations = new ArrayList<StoreLocation>();

        
        public Carpart() {
        }
        
        public String getPartNumber() {
            return partNumber;
        }

        public void setPartNumber(String partNumber) {
            this.partNumber = partNumber;
        }

        public String getLine() {
            return line;
        }

        public void setLine(String line) {
            this.line = line;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getImageName() {
            return imageName;
        }

        public void setImageName(String imageName) {
            this.imageName = imageName;
        }

        public Map<String, String> getDetailsMap() {
            return detailsMap;
        }

        public void setDetailsMap(Map<String, String> detailsMap) {
            this.detailsMap = detailsMap;
        }

        public List<CarpartDetail> getCarpartDetailsList() {
            return carpartDetailsList;
        }

        public void setCarpartDetailsList(List<CarpartDetail> carpartDetailsList) {
            this.carpartDetailsList = carpartDetailsList;
        }

        public List<StoreLocation> getStoreLocations() {
            return storeLocations;
        }

        public void setStoreLocations(List<StoreLocation> storeLocations) {
            this.storeLocations = storeLocations;
        }

        @Override
        public String toString() {
            return "Carpart [partNumber=" + partNumber + ", title=" + title + ", description=" + description + ", imageName=" + imageName
                    + "]";
        }
}
