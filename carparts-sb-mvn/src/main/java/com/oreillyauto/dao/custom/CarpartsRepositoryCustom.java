package com.oreillyauto.dao.custom;

import java.util.List;

import com.oreillyauto.domain.Carpart;

public interface CarpartsRepositoryCustom {
    public Carpart getCarpartByPartNumber(String partNumber);
    public List<Carpart> getCarparts();
    public Carpart getCarpart(String partNumber) throws Exception;
}
